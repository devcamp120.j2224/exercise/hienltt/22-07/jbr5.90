package com.devcamp.music_api.model;

import java.util.List;

public class Artist extends Composer{
    private List<Album> albums;

    public Artist(String firstname, String lastname, String stagename, List<Album> albums) {
        super(firstname, lastname, stagename);
        this.albums = albums;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }
    
}
