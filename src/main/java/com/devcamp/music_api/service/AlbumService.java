package com.devcamp.music_api.service;

import java.util.ArrayList;

import com.devcamp.music_api.model.Album;

public class AlbumService {
    Album album1 = new Album("name1", new ArrayList<String>() {
        {
            add("song 1");
            add("song 2");
        }
    });
    Album album2 = new Album("name2", new ArrayList<String>() {
        {
            add("song 3");
            add("song 4");
        }
    });
    Album album3 = new Album("name3", new ArrayList<String>() {
        {
            add("song 5");
            add("song 6");
        }
    });
    Album album4 = new Album("nam4", new ArrayList<String>() {
        {
            add("song 7");
            add("song 8");
        }
    });
    Album album5 = new Album("name5", new ArrayList<String>() {
        {
            add("song 9");
            add("song 10");
        }
    });
    Album album6 = new Album("name6", new ArrayList<String>() {
        {
            add("song 11");
            add("song 12");
        }
    });
    Album album7 = new Album("name7", new ArrayList<String>() {
        {
            add("song 13");
            add("song 14");
        }
    });
    Album album8 = new Album("name8", new ArrayList<String>() {
        {
            add("song 15");
            add("song 16");
        }
    });
    Album album9 = new Album("name9", new ArrayList<String>() {
        {
            add("song 17");
            add("song 18");
        }
    });
    Album album10 = new Album("name10", new ArrayList<String>() {
        {
            add("song 19");
            add("song 20");
        }
    });

    public ArrayList<Album> getAllAlbums(){
        ArrayList<Album> lAlbums = new ArrayList<>();
        lAlbums.add(album1);
        lAlbums.add(album2);
        lAlbums.add(album3);
        lAlbums.add(album4);
        lAlbums.add(album5);
        lAlbums.add(album6);
        lAlbums.add(album7);
        lAlbums.add(album8);
        lAlbums.add(album9);
        lAlbums.add(album10);
        return lAlbums;
    }
    public ArrayList<Album> getAlbums1(){
        ArrayList<Album> lAlbums = new ArrayList<>();
        lAlbums.add(album1);
        lAlbums.add(album2);
        lAlbums.add(album3);
        return lAlbums;
    }
    public ArrayList<Album> getAlbums2(){
        ArrayList<Album> lAlbums = new ArrayList<>();
        lAlbums.add(album4);
        lAlbums.add(album5);
        lAlbums.add(album6);
        return lAlbums;
    }
    public ArrayList<Album> getAlbums3(){
        ArrayList<Album> lAlbums = new ArrayList<>();
        lAlbums.add(album7);
        lAlbums.add(album8);
        lAlbums.add(album9);
        lAlbums.add(album10);
        return lAlbums;
    }
}
