package com.devcamp.music_api.service;

import java.util.ArrayList;

import com.devcamp.music_api.model.Artist;
import com.devcamp.music_api.model.Band;
import com.devcamp.music_api.model.BandMember;
import com.devcamp.music_api.model.Composer;

public class ComposerService {
    AlbumService albumService = new AlbumService();
    Artist artist1 = new Artist("Nguyen", "A", "stagename1", albumService.getAlbums1());   
    Artist artist2 = new Artist("Nguyen", "B", "stagename2", albumService.getAlbums2());   
    Artist artist3 = new Artist("Nguyen", "C", "stagename3", albumService.getAlbums3()); 

    BandMember bandMember1 = new BandMember("Band", "1", "new", "ghita");
    BandMember bandMember2 = new BandMember("Band", "2", "new1", "ỏgan");
    BandMember bandMember3 = new BandMember("Band", "3", "new2", "drum");
    BandMember bandMember4 = new BandMember("Band", "4", "new3", "violon");
    BandMember bandMember5 = new BandMember("Band", "5", "new4", "piano");
    BandMember bandMember6 = new BandMember("Band", "6", "new5", "trumpet");

    public ArrayList<Composer> getAllComposer(){
        ArrayList<Composer> lComposers = new ArrayList<>();
        lComposers.add(artist1);
        lComposers.add(bandMember1);
        lComposers.add(bandMember2);
        lComposers.add(artist2);
        lComposers.add(bandMember3);
        lComposers.add(bandMember4);
        lComposers.add(artist3);
        lComposers.add(bandMember5);
        lComposers.add(bandMember6);
        return lComposers;
    }

    Band band1 = new Band("bandname1", new ArrayList<BandMember>() {
        {
            add(bandMember1);
            add(bandMember2);
        }
    }, albumService.getAlbums1());

    Band band2 = new Band("bandname1", new ArrayList<BandMember>() {
        {
            add(bandMember3);
            add(bandMember4);
        }
    }, albumService.getAlbums3());
    
}
